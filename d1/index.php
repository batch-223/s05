<?php
// $_GET and $_POST are "super global" variables in PHP.
//Always accessible regardless the scope

echo $_GET['index'];
var_dump($_GET);



if (isset($_GET['index'])) {
    echo $_GET['index'];
    var_dump($_GET);
}
if (isset($_POST['index'])) {
    echo $_POST['index'];
    var_dump($_POST);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S05: Client-Server Communication (GET and POST)</title>
</head>

<body>
    <h1>Task index from GET</h1>

    Email: <input type="text" name="email">
    <br>
    Passwrod: <input type="password">

    <form method="GET">
        <select name="index" required>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <select>
                <button>GET</button>
    </form>

    <h1>Task index from POST</h1>
    <form method="POST">
        <select name="index" required>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>

            <select>
                <button>POST</button>
    </form>
</body>

</html>