<?php

// session_start() is also added in the server.php so it is also included in the current session.
session_start();

// echo $_POST['description'];
// We can check that we have able to access the value of our session variable.
// echo $_SESSION['greet'];

class TaskList
{

    // Add task
    public function add($description)
    {
        $newTask = (object)[
            'description' => $description,
            'isFinished' => false
        ];

        // If there is no added tasks yet.
        if ($_SESSION['tasks'] === null) {
            $_SESSION['tasks'] = array();
        }

        // Then $newTask will be added in the $_SESSION['tasks'] variable.
        array_push($_SESSION['tasks'], $newTask);
    }

    // Update a Task
    // The update task will be needing three parameters:
    // $id for searching specific task.
    // $description and $isFinished

    public function update($id, $description, $isFinished)
    {
        // task[$id] same with this variable 
        $_SESSION['tasks'][$id]->description = $description;

        $_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
    }

    public function remove($id)
    {
        // array_splice(array, startDel, length, newArrElement)
        array_splice($_SESSION['tasks'], $id, 1);
    }

    // Remove all the task
    public function clear()
    {
        session_destroy();
    }
}

// taskList is instantiated from the TaskList() class to have access with its method.
$taskList = new TaskList();

// This will handle the action sent by the user.
if ($_POST['action'] === 'add') {
    $taskList->add($_POST['description']);
} else if ($_POST['action'] === 'update') {
    $taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
} else if ($_POST['action'] === 'remove') {
    $taskList->remove($_POST['id']);
} else if ($_POST['action'] === 'clear') {
    $taskList->clear();
}

// it will redirect us to the index file upon sending the request.
header('Location: ./index.php');
